#!/usr/bin/env python

import numpy as np 
import matplotlib.pyplot as plt
import matplotlib as mpl
import networkx as nx

def Tau():
    return np.pi*2
    
def BlackNWhite():
    # define the colors
    return mpl.colors.ListedColormap(['w', 'k'])

def fAdd(a,b): # check validity of a+b = 0
    ValuesOK = False
    DerivOK = False
    if a[0]==0 and b[0]==0:
        ValuesOK = True
    else:
        if a[0]==+1 and b[0]==-1:
            ValuesOK = True
        elif a[0]==-1 and b[0]==+1:
            ValuesOK = True
    if ValuesOK:
        if a[1]==0 and b[1]==0:
            DerivOK = True
        else:
            if a[1]==+1 and b[1]==-1:
                DerivOK = True
            elif a[1]==-1 and b[1]==+1:
                DerivOK = True
                
    return (ValuesOK and DerivOK)
def fSum(v): # check validity of a+b = 0
    ValuesOK = False
    DerivOK = False
    N = len(v)
    v0 = [] 
    v1 =  []
    for i in range(N):
        v0.append(v[i][0])
        v1.append(v[i][1])
    if all(elem == 0 for elem in v0):
        ValuesOK = True
    else:
        if ( any(elem == +1 for elem in v0) and any(elem == -1 for elem in v0) ):
            ValuesOK = True
    if all(elem == 0 for elem in v1):
        DerivOK = True
    else:
        if ( any(elem == +1 for elem in v1) and any(elem == -1 for elem in v1) ):
            DerivOK = True
                
    return (ValuesOK and DerivOK)

def fBilinearSum(a,x,y):
    
    ValuesOK = False
    DerivOK = False
    N = len(a)
    z0 = [] 
    z1 =  []
    for i in range(N):
        z0.append(x[i][0]*y[i][0]*a[i])
        z1.append(x[i][1]*y[i][0]*a[i])
        z1.append(x[i][0]*y[i][1]*a[i])
        pass
    
    #print(a,x,y,z0,z1)
    if all(elem == 0 for elem in z0):
        ValuesOK = True
        pass
    else:
        if any(np.isnan(elem) for elem in z0):
            ValuesOK = True
            pass
        elif ( any(elem == +1 for elem in z0) and any(elem == -1 for elem in z0) ):
            ValuesOK = True
            pass
        
    if all(elem == 0 for elem in z1):
        DerivOK = True
    else:
        if any(np.isnan(elem) for elem in z1):
            DerivOK = True
            pass
        if ( any(elem == +1 for elem in z1) and any(elem == -1 for elem in z1) ):
            DerivOK = True
            pass
        
    return (ValuesOK and DerivOK)

def fProductSum(a,x): # check validity of sum_i a[i]*x[i] = 0
    ValuesOK = False
    DerivOK = False
    N = len(x)
    z0 = [] 
    z1 =  []
    for i in range(N):
        z0.append(x[i][0]*a[i])
        z1.append(x[i][1]*a[i])
    if all(elem == 0 for elem in z0):
        ValuesOK = True
        pass
    else:
        if any(np.isnan(elem) for elem in z0):
            ValuesOK = True
            pass
        elif ( any(elem == +1 for elem in z0) and any(elem == -1 for elem in z0) ):
            ValuesOK = True
            pass
    if all(elem == 0 for elem in z1):
        DerivOK = True
    else:
        if any(np.isnan(elem) for elem in z1):
            DerivOK = True
            pass
        if ( any(elem == +1 for elem in z1) and any(elem == -1 for elem in z1) ):
            DerivOK = True
            pass
                
    return (ValuesOK and DerivOK)

def fMultiply(a,b,c): # check validity of a*b = c
    ValuesOK = False
    if (a[0]==0 or b[0]==0):
        if c[0]==0:
            ValuesOK = True
    else:
        if a[0]==b[0] and c[0]==+1:
            ValuesOK = True
        elif (not a[0]==b[0]) and c[0]==-1:
            ValuesOK = True
    
    # derivative = S1 + S2 = Da . b + a . Db = Dc
    # Assignment of sign to term S1 and S2
    if (a[0]==0 or b[1]==0):
        S1 = 0
    else:
        if a[0]==b[1]:
            S1 = +1
        else:
            S1 = -1
    if (b[0]==0 or a[1]==0):
        S2 = 0
    else:
        if a[1]==b[0]:
            S2 = +1
        else:
            S2 = -1
    
    if (S1==0 and S2==0):
        if c[1]==0:
            DerivOK = True
        else:
            DerivOK = False
    elif (S1>=0 and S2>=0) and c[1]==+1:
        DerivOK = True
    elif (S1<=0 and S2<=0) and c[1]==-1:
        DerivOK = True
    elif (S1==+1 and S2==-1) or (S1==-1 and S2==+1):
        DerivOK = True
    else:
        DerivOK = False
            
    return (ValuesOK and DerivOK)

def fIsotone0(a,b): # check validity of a = M0+(b)
    ValuesOK = False
    DerivOK = False
    if a[0]==b[0]:
        ValuesOK = True
    if ValuesOK:
        if np.isnan(a[1]) or np.isnan(b[1]):
            DerivOK = True
            pass
        elif a[1]==b[1]:
            DerivOK = True
            
    return (ValuesOK and DerivOK)

def fDifferential(x,dxdt,a): # check validity of (x)' = dxdt
    OK = False
    if x[1]==a*dxdt[0]:
        OK = True
        pass
    return OK

def fAmatrix(solutions):
    nSol = len(solutions)
    A = np.zeros([nSol,nSol])
    for source in range(nSol):
        for target in range(nSol):
            SolS = solutions[source]
            SolT = solutions[target]
            
            if source==target:
                xMatch = True
                for key in SolS.keys():
                    SolVar = SolS.get(key)
                    if (SolVar[0]==0 and (not SolVar[1]==0)):
                        xMatch = False
                        pass
            else:
                #print(SolS,SolT)
                xMatch=True
                for var in SolS:
                    #print(var)
                    VarS = SolS.get(var)
                    VarT = SolT.get(var)
                    if (VarS[0]==VarT[0]) and (VarS[1]==VarT[1]):
                        if VarS[0]==0 and not(VarS[1]==0):
                            xMatch=False
                            pass
                        pass
                    elif VarS[1]==0:
                        if VarS[0]==VarT[0]:
                            pass
                        else:
                            xMatch=False
                            #print('Rule0')
                        pass
                    elif VarS[0]==0:
                        if (VarS[1]==VarT[0]) and (VarS[1]==VarT[1]):
                            pass
                        else:
                            xMatch=False
                            #print('Rule1')
                        pass
                    elif not( (VarS[0]==0) or (VarS[1]==0) ):
                        if (VarS[0]==VarT[0]) and VarT[1]==0:
                            pass
                        elif (VarS[1]==VarT[1] and not(VarS[0]==VarS[1])) and VarT[0]==0:
                            pass
                        elif not(VarS[0]==VarS[1]) and (VarT[0]==0 and VarT[1]==0):
                            pass
                        else:
                            xMatch=False
                            #print('Rule2')
                        pass
                    else:
                        xMatch=False
                        #print('RuleX')
                        pass
                
            A[source,target] = xMatch
    return A

def fGraph(solutions):
    nSol = len(solutions)
    A = np.zeros([nSol,nSol])
    G = nx.DiGraph()
    for source in range(nSol):
        G.add_node(source,state=solutions[source])
        pass
    transit = np.zeros(nSol)
    terminal = np.zeros(nSol)
        
    for source in range(nSol):
        SolS = solutions[source]
        xTrans = False
        xTerminal = True
        for key in SolS.keys():
            SolVar = SolS.get(key)
            if (SolVar[0]==0 and (SolVar[1]==+1 or SolVar[1]==-1)):
                xTrans = True
                pass
            if (SolVar[1]==0):
                xTerminal = False
                pass
            pass
        transit[source] = xTrans
        terminal[source] = xTerminal
        xMatch = np.logical_not(xTrans)
        A[source,source] = xMatch
        pass
    
    for source in range(nSol):
        for target in range(nSol):
            SolS = solutions[source]
            SolT = solutions[target]
            
            if source==target:
                pass
            else:
                xMatch = True ;
                #print(SolS,SolT)
                for var in SolS:
                    #print(var)
                    VarS = SolS.get(var)
                    VarT = SolT.get(var)
                    if (np.isnan(VarS[1]) or np.isnan(VarT[1])):
                        pass 
                    elif transit[source] and (VarS[0]!=VarT[0]) and (VarS[1]!=VarT[1]):
                        xMatch=False
                        pass
                    elif (VarS[0]==VarT[0]) and (VarS[1]==VarT[1]):
                        if VarS[0]==0 and not(VarS[1]==0):
                            xMatch=False
                            pass
                        pass
                    elif VarS[1]==0:
                        if VarS[0]==VarT[0]:
                            pass
                        else:
                            xMatch=False
                            #print('Rule0')
                        pass
                    elif VarS[0]==0:
                        if (VarS[1]==VarT[0]) and (VarS[1]==VarT[1]):
                            pass
                        else:
                            xMatch=False
                            #print('Rule1')
                        pass
                    else:
                        if (VarS[0]==VarT[0]) and VarT[1]==0:
                            pass
                        elif (VarS[1]==VarT[1] and not(VarS[0]==VarS[1])) and VarT[0]==0:
                            pass
                        elif not(VarS[0]==VarS[1]) and (VarT[0]==0 and VarT[1]==0):
                            pass
                        else:
                            xMatch=False
                            #print('Rule2')
                        pass
                    pass
                A[source,target] = xMatch 
                pass
            pass
        pass
                
    return A

def fState2String(x):
    if x[0]==+1:
        d0 = '+'
        pass
    elif x[0]==-1:
        d0 = '-'
        pass
    else:
        d0 = '0'
        pass
    if x[1]==+1:
        d1 = '\u25b2'
    elif x[1]==-1:
        d1 = '\u25bc'
    else:
        d1 = '\u25cf'
        pass
    return ('('+d0+','+d1+')')

def fPlotEnvisionGraph(solutions,A):
    nSol = len(solutions)
    posX = np.cos((np.arange(nSol)/nSol+1/8)*Tau())
    posY = np.sin((np.arange(nSol)/nSol+1/8)*Tau())
    
    fig, ax = plt.subplots(figsize=(8,8))
    xTrans = (np.diag(A)==0)
    plt.plot(posX[~xTrans],posY[~xTrans],'ko',MarkerSize=12,markerfacecolor='k')
    plt.plot(posX[xTrans],posY[xTrans],'ko',MarkerSize=12,markerfacecolor='w')
    for source in range(nSol):
        strNodeLabel = ''
        print(solutions[source].keys())
        for key in solutions[source].keys():
            strNodeLabel = strNodeLabel+fState2String(solutions[source].get(key))
        plt.text(posX[source]*1.1,posY[source]*1.1, strNodeLabel, horizontalalignment='center',verticalalignment='center',fontsize=12,color='r')
        for target in range(nSol):
            if A[source,target] and not(source==target):
                plt.arrow(posX[source],posY[source],posX[target]-posX[source],posY[target]-posY[source], head_width=0.05, head_length=0.05, fc='k', ec='k',length_includes_head=True)
                pass
            pass
        pass
    return

                
def fPlotEnvisionMatrix(solutions,A):
    fig, ax = plt.subplots(figsize=(8,8))
    ax.imshow(A, interpolation='none', cmap=BlackNWhite())
    return
    
def sign2str(x,*,direc=False):
    
    if np.isnan(x):
        y = '?' ;
        pass
    else:
        if x==0:
            if direc:
                y = '\u2022';
                pass
            else:
                y = '0' ;
                pass
            pass
        else:
            if x>0:
                if direc:
                    y = '\u2191' ;
                    pass
                else:
                    y = '+' ;
                    pass
                pass
            else:
                if direc:
                    y = '\u2193' ;
                    pass
                else:
                    y = '-'
                    pass
                pass
            pass
        pass
    
    return y


def plotGraph(A,solutions,*,ax=[],XY=[],description=[],offset=0):
    DG = nx.DiGraph()
    [I,J] = np.shape(A)
    mapping = dict()
    if np.prod(np.shape(description))==0:
        description = dict()
        for i in range(I):
            description[i] = ' ';
            pass
        pass
    position = dict()
    transition = np.zeros(I) 
    terminal = np.zeros(I) 
    if np.prod(np.shape(XY))==0:
        XY = np.random.randn(I,2)
        pass
    
    for i in range(I):
        DG.add_node(i)
        lbl = str(i) ; 
        mapping[i] = lbl
        position[lbl] = [XY[i,0] , XY[i,1]]; 
        transition[i] = np.logical_not(A[i,i])
        terminal[i] =  (np.sum(A[i,:]==1)==1 and A[i,i])
        pass
    
    for i in range(I):
        for j in range(J):
            if A[i,j]:
                DG.add_edges_from([(i,j)])
            pass
        pass
    
    if ax==[]:
        fig, ax = plt.subplots(nrows=1, ncols=1,figsize=(3,3),dpi=250)
        pass 
    
    plt.axes(ax) 
    DG = nx.relabel_nodes(DG, mapping)
    gplot0 = nx.draw(DG,node_size =240,font_size =10,pos=position, 
                              edge_color ='black', 
                              edgelist = [],
                              nodelist = [ mapping[i] for i in np.where(terminal)[0]  ],
                              node_color= 'black',alpha =1,
                              arrowstyle='->',with_labels=False, font_weight='normal', width =2)
    gplot1 = nx.draw(DG,node_size =200,font_size =10,pos=position, 
                              edge_color ='orange', 
                              node_color= 'orange',alpha =1, 
                              arrowstyle='->',with_labels=True, font_weight='normal', width =0.5)
    
    gplot2 = nx.draw(DG,node_size =180,font_size =10,pos=position, 
                              edge_color ='orange', 
                              edgelist = [],
                              nodelist = [ mapping[i] for i in np.where(transition)[0]  ],
                              node_color= 'white',alpha =1,
                              arrowstyle='->',with_labels=False, font_weight='normal', width =0.5)
    gplot3 = nx.draw(DG,node_size =180,font_size =10,pos=position, 
                              edge_color ='orange', 
                              edgelist = [],
                              nodelist = [ mapping[i] for i in np.where(1-transition)[0]  ],
                              node_color= 'orange',alpha =1,
                              arrowstyle='->',with_labels=False, font_weight='normal', width =0.5)
    
    for index,label in enumerate(position): 
        plt.text(position[label][0],position[label][1]-offset,description[index],
                 horizontalalignment ='center',
                 verticalalignment ='top',
                fontsize =4)
        pass
    return 

def printsolutions(solutions,*,keys=[]):
    
    I = len(solutions)
    if keys==[]:
        sol = solutions[0]
        keys=sol.keys()
        pass
    
    I = len(solutions)
    description = dict()
    for i in range(I):
        sol = solutions[i] ;
        label = ''
        #print(sol.keys())
        for key in keys: 
            v = sign2str(sol[key][0])
            vdot = sign2str(sol[key][1],direc=True)
            label = label+str(key)+': ['+v+','+vdot+']; '
            pass
        description[i] = label
        pass
    return description
